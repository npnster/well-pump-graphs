class DatabaseAccess {
    constructor() {
        this._message = "hi";
        this.authDomain = 'well-pump-monitor.firebaseapp.com'
        this.apiKey = 'AIzaSyA2g4YDDCL2RJCJOiU7PJKVd6w-M3KMn0E'
        this.databaseURL = 'well-pump-monitor.firebaseio.com'
        this.snapshot = null;
        let config = {
            apiKey: this.apiKey,
            authDomain: this.authDomain,
            databaseURL: this.databaseURL
        }
        firebase.initializeApp(config)
    }

    get events() {
        return Object.values(this.snapshot.val());
    }

    getDurations(type) {
        const durations = []
        this.events.forEach((e, i) => {
            if (e.event_type == type && this.events[i + 1]) {
                console.log(this.events[i + 1].event_type, e.event_type)
                const duration = (this.events[i + 1].event_type != e.event_type) ? this.events[i + 1].event_time - e.event_time : -10000
                const duration_point = { epochTime: e.event_time, duration: duration }
                durations.push(duration_point)
            }

        })
        return durations
    }

    get onDurations() {
        return this.getDurations("on")
    }

    buildGraph() {
        firebase.database().ref('/well_events').once('value').then(snapshot => {
            this.snapshot = snapshot;
            let onDurations = this.onDurations;
            google.charts.load('current', { packages: ['corechart', 'bar'] });
            google.charts.setOnLoadCallback(() => {
                const data = new google.visualization.DataTable();
                data.addColumn('datetime', 'Date');
                data.addColumn('number', 'Duration');
                onDurations.forEach((d) => {
                    data.addRow([new Date(d.epochTime), d.duration / 1000.0])
                });

                const options = {
                    width: 1500,
                    height: 500,
                    legend: { position: 'none' },
                    // enableInteractivity: false,
                    chartArea: {
                        width: '85%'
                    },
                    title: 'Well pump events',
                    hAxis: {
                        title: 'Date',
                        gridlines: {
                            count: -1,
                            units: {
                                days: { format: ['MMM dd'] },
                                // hours: { format: ['HH:mm', 'ha'] },
                            }
                        },
                        minorGridlines: {
                            count: -1,
                            units: {
                                hours: { format: ['hh:mm:ss a', 'ha'] },
                                // minutes: { format: ['HH:mm a Z', ':mm'] }
                            }
                        }
                    },
                    vAxis: {
                        title: 'Duration (seconds)'
                    }
                };
                const durationChart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                durationChart.draw(data, options);
            });
        });
    }
}

const wellDb = new DatabaseAccess();
wellDb.buildGraph();